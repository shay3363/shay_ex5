#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>


class Menu
{
public:

	Menu();
	~Menu();
	char getChoice() const;
	void makeShapes();
	void editShapes();
	void printAllShape();
	void deleteAllShapes();
	void mainMenu();
	void removeShape(int index);
private:
	vector<Shape*> _shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};