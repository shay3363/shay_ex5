#include "Polygon.h"
Polygon::Polygon(const string& type, const string& name) : Shape(name,type){}
Polygon::~Polygon() {}
double Polygon::getPerimeter() const
{
	int i = 0;
	double perimeter = 0;
	for (i = 0; i < (this->_points.size()-1); i++)
	{
		perimeter += _points[i].distance(_points[i + 1]);
	}
	return perimeter;
}
void Polygon::move(const Point& other)
{
	int i = 0;
	for (i = 0; i < this->_points.size(); i++)
	{
		this->_points[i] += other;
	}
}