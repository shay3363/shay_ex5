#include "Menu.h"/


Menu::Menu() 
{
	_board  = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}

Menu::~Menu()
{
	_disp->close();
	delete _board;
	delete _disp;
}
char Menu::getChoice() const
{
	int choice = 0;
	cin >> choice;
	return choice;
}
void Menu::makeShapes()
{
	double radius = 0;
	double length = 0;
	double width = 0;
	bool numLeagal = false;
	int i = 0;
	int x[3] = { 0 };
	int y[3] = { 0 };
	string name = "";
	std::cout << "Enter 0 to add a circle.\nEnter 1 to add an arrow.\nEnter 2 to add a triangle.\nEnter 3 to add a rectangle." << endl;
	while (!numLeagal)
	{
		numLeagal = true;
		switch (this->getChoice())
		{
		case 0:
			cout << "Please enter X:" << endl;
			cin >> x[0];
			cout << "Please enter Y:" << endl;
			cin >> y[0];
			cout << "Please enter radius:" << endl;
			cin >> radius;
			cout << "Please enter the name of the shape:" << endl;
			cin >> name;
			this->_shapes.push_back(new Circle(Point(x[0], y[0]), radius, "circle", name));
			this->_shapes[this->_shapes.size() - 1]->draw(*_disp, *_board);
			system("cls");
			break;
		case 1:
			for (i = 0; i < 2; i++)
			{
				cout << "Enter the X of point number: " << i << endl;
				cin >> x[i];
				cout << "Enter the Y of point number: " << i << endl;
				cin >> y[i];
			}
			cout << "Please enter the name of the shape:" << endl;
			cin >> name;
			this->_shapes.push_back(new Arrow(Point(x[0], y[0]), Point(x[1], y[1]), "arrow", name));
			this->_shapes[this->_shapes.size() - 1]->draw(*_disp, *_board);
			system("cls");
			break;
		case 2:
			for (i = 0; i < 3; i++)
			{
				cout << "Enter the X of point number: " << i << endl;
				cin >> x[i];
				cout << "Enter the Y of point number: " << i << endl;
				cin >> y[i];
			}
			cout << "Please enter the name of the shape:" << endl;
			cin >> name;
			if (x[0] != x[1] || x[0] != x[2] || x[2] != x[1] && y[0] != y[1] || y[0] != y[2] || y[2] != y[1])
			{
				this->_shapes.push_back(new Triangle(Point(x[0], y[0]), Point(x[1], y[1]), Point(x[2], y[2]), "Triangle", name));
				this->_shapes[this->_shapes.size() - 1]->draw(*_disp, *_board);
				system("cls");
			}
			else
			{
				cout << "The points entered create a line." << endl;
				cout << "press any key to continue...";
				getchar();
				system("cls");
			}
			break;
		case 3:
			cout << "Enter the X of the to left corner: " << endl;
			cin >> x[0];
			cout << "Enter the X of the to left corner: " << endl;
			cin >> y[0];
			cout << "Please enter the lenght of the shape:" << endl;
			cin >> length;
			cout << "Please enter the width of the shape:" << endl;
			cin >> width;
			if (width > 0 && length > 0)
			{
				this->_shapes.push_back(new myShapes::Rectangle(Point(x[0], y[0]), length, width, "Rectangle", name));
				this->_shapes[this->_shapes.size() - 1]->draw(*_disp, *_board);
				system("cls");
			}
			else
			{
				cout << "Length or Width can't be 0." << endl;
				cout << "press any key to continue...";
				system("cls");
			}
			break;
		default:
			numLeagal = false;
			break;
		}
	}

}
void Menu::printAllShape() 
{
	int i = 0;
	for (i = 0; i < this->_shapes.size(); i++)
	{
		this->_shapes[i]->draw(*_disp,*_board);
	}
}
void Menu::removeShape(int index)
{
	this->_shapes.erase(this->_shapes.begin() + index-1);
}
void Menu::editShapes()
{
	double x = 0;
	double y = 0;
	int i = 0;
	int shapeIndex = 0;
	int choice = 0;
	bool leagalChoice = false;
	if (this->_shapes.size() >= 1)
	{
		for (i = 0; i < this->_shapes.size(); i++)
		{
			std::cout << "Enter " << i << "for" << this->_shapes[i]->getName() << "(" << this->_shapes[i]->getType() << ")" << endl;
		}
		shapeIndex = this->getChoice();
		std::cout << "Enter 0 to move the shape\nEnter 1 to get its details.\nEnter 2 to remove the shape." << endl;
		choice = this->getChoice();
		while (!leagalChoice)
		{
			leagalChoice = true;
			switch (choice)
			{
			case 0:
				system("cls");
				cout << "Please enter the X moving scale:" << endl;
				cin >> x;
				cout << "Please enter the Y moving scale:" << endl;
				cin >> y;
				this->_shapes[shapeIndex]->clearDraw(*_disp,*_board);
				this->_shapes[shapeIndex]->move(Point(x, y));
				this->printAllShape();
				break;
			case 1:
				cout << this->_shapes[shapeIndex]->getType() << "   " << this->_shapes[shapeIndex]->getName() << "  " << this->_shapes[shapeIndex]->getArea() << "  " << this->_shapes[shapeIndex]->getPerimeter() << endl;
				cout << "Press any key to continue . . .";
				getchar();
				system("cls");
				break;
			case 2:
				this->_shapes[shapeIndex]->clearDraw(*_disp, *_board);
				this->removeShape(shapeIndex);
				this->printAllShape();
				break;
			default:
				leagalChoice = false;
				break;
			}
		}
	}
}
void Menu::deleteAllShapes()
{
	int i = 0;
	Shape* curr = 0;
	for (i = this->_shapes.size() - 1; i > 0; i--)
	{
		delete this->_shapes[i];
		this->_shapes.pop_back();
	}
}
void Menu::mainMenu()
{
	int choice = 0;
	while (choice != 3)
	{
		std::cout << "Enter 0 to add a new shape.\nEnter 1 to modify or get information from a current shape.\nEnter 2 to delete all of the shapes.\nEnter 3 to exit." << endl;
		choice = getChoice();
		switch (choice)
		{
		case 0:
			system("cls");
			this->makeShapes();
			break;
		case 1:
			system("cls");
			this->editShapes();
			break;
		case 2:
			system("cls");
			this->deleteAllShapes();
			break;
		default:
			system("cls");
			break;
		}
	}
	this->deleteAllShapes();
}