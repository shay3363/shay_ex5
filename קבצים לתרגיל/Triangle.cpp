#include "Triangle.h"
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name) : Polygon(type, name)
{
	_points.push_back(Point(a));
	_points.push_back(Point(b));
	_points.push_back(Point(c));
}
Triangle::~Triangle() {}
void Triangle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char GREEN[] = { 0, 255, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), GREEN, 100.0f).display(disp);
}

void Triangle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	board.draw_triangle(_points[0].getX(), _points[0].getY(),
		_points[1].getX(), _points[1].getY(),
		_points[2].getX(), _points[2].getY(), BLACK, 100.0f).display(disp);
}
double Triangle::getArea() const
{
	return 0.5*(this->_points[0].getX() * (this->_points[2].getY() - this->_points[1].getY()) + this->_points[1].getX() * (this->_points[0].getY() - this->_points[2].getY()) + this->_points[2].getX() * (this->_points[1].getY() - this->_points[0].getY())); //this is a formula for the area of the triangle with three points of it.
}