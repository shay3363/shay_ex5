#include "Shape.h"
Shape::Shape(const string& name, const string& type)
{
	this->_name = name;
	this->_type = type;
}
Shape::~Shape() {}
void Shape::printDetails() const
{
	std::cout << "name: " << this->_name << "\n";
	std::cout << "type: " << this->_type << "\n";
}
string Shape::getType() const
{
	return this->_type;
}
string Shape::getName() const
{
	return this->_name;
}
